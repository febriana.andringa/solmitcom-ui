import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';

Vue.use(VueRouter);

// configure router
const router = new VueRouter({
  routes, // short for routes: routes
  linkActiveClass: 'active',
  scrollBehavior: (to, from ,savedPosition) => {
    if (savedPosition) {
      return savedPosition;
    }
    if (to.hash) {
      return { selector: to.hash };
    }
    return { x: 0, y: 0 };
  }
});

router.beforeEach((to, from, next) => {
  const isAuth = localStorage.getItem('token')
  const isRole = localStorage.getItem('role')

  // check apabila admin tidak memiliki auth
  if (to.matched[0].name === 'adminAuth' && !isAuth) {
    next({
      name: 'login'
    })
  }

  // check apabila user mengakses route admin
  // if (to.matched[0].name === 'adminAuth' && isAuth && isRole !== 1) {
  //   next({
  //     name: 'login'
  //   })
  // }

  // check apabila admin sudah login dan mengakses menu login/register
  if ((to.name === 'login' || to.name === 'register') && isAuth && isRole !== 1) {
    next({
      name: 'dashboard'
    })
  }

  next()
})

export default router;
