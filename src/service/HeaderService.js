class HeaderService {
    get(token = ''){
        if (token != '') {
            var headers = {
                "Authorization": localStorage.getItem('type') + " " + localStorage.getItem('token'),
                "Content-Type" : "application/json",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "*",
                "Access-Control-Allow-Headers": "*"
            }
        }else{
            var headers = {
                'Content-Type' : 'application/json'
            }
        }
        return headers
    }
}

export default new HeaderService();