import axios from 'axios'
import header from '../HeaderService'

class CategoryService {

    show(params) {
        return axios.post(process.env.VUE_APP_URL + '/category/show', params, {
            headers: header.get('token')
        })
    }

    create(params) {
        return axios.post(process.env.VUE_APP_URL + '/category/create', params, {
            headers: header.get('token')
        })
    }

    delete(id) {
        var params = {id:id}
        return axios.post(process.env.VUE_APP_URL + "/category/delete", params, {
            headers: header.get('token')
        })
    }
}
export default new CategoryService();

