import axios from 'axios'
import header from '../HeaderService'

class ArticleService {

    show(params) {
        console.log(params)
        return axios.post(process.env.VUE_APP_URL + "/article/show", params, {
            headers: header.get('token')
        })
    }

    create(params) {
        console.log(params)
        // return axios.post(process.env.VUE_APP_URL + "/article/create", params, {
        //     headers: header.get('token')
        // })
    }

    delete(id) {
        var params = {id:id}
        return axios.post(process.env.VUE_APP_URL + "/article/delete", params, {
            headers: header.get('token')
        })
    }
}
export default new ArticleService();

