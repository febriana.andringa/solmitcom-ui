import axios from 'axios'
class AuthService {
    login(params) {
        return axios.create({
            baseURL: process.env.VUE_APP_URL,
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8;application/json'
            }
        }).post("/login", params)
    }

    delete(id) {
        var params = {id:id}
        return axios.create({
            baseURL: process.env.VUE_APP_URL,
            headers: {
                'Authorization': JSON.parse(localStorage('type')) + ' ' + JSON.parse(localStorage('token')),
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8;application/json'
            }
        }).post("/article/delete", params)
    }
}
export default new AuthService();

